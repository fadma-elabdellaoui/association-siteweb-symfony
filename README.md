# Association-SiteWeb-Symfony


## 📑 Description du projet
En résumé, le site permettrait aux utilisateurs de consulter et de créer des offres de stages, de voir les événements organisés par l’association, et de gérer les partenaires et les membres du bureau. 

Les administrateurs auraient des droits supplémentaires pour gérer le site, comme la création et la suppression d’événements,
l’ajout et la suppression de partenaires, etc.

Le site est construit en utilisant Symfony, et responsive pour une utilisation sur téléphone et ordinateur.

### 🛠️ Technologies utilisées
***

- Docker
- Symfony (back-end)
- EasyAdmin (back office)
- Postgres (base de données sql)
- Bootstrap (front-end)

## ⚙ Installation
***
**Requirements:** installer Docker sur votre machine.


### 🔹 Copier & Lancer le projet sur votre machine

Après l'installation de docker, cloner le projet sur votre machine avec :

```
git clone https://gitlab.com/fadma-elabdellaoui/association-siteweb-symfony.git
```

Une fois le projet cloné :

- Utilisez ces commandes sur votre terminal :
- Se positionner dans le répertoire du projet au même niveau que `docker-compose.yml`
  - `docker-compose build`
  - `docker-compose up -d`
  - `docker exec -ti web bash`
  - `cd projet`
  - `symfony composer install`
  - `symfony console make:migration`
  - `symfony console doctrine:migrations:migrate`
  - `symfony console doctrine:fixture:load`
  - `npm install`
  - `npm run dev`
  - `symfony server:start --no-tls --d`

Vous pouvez maintenant accéder au site à l'adresse : http://localhost:5000

Vous pouvez aussi accéder à la base avec Adminer : http://localhost:18080/


## Modélisation
***
![Modélisation du projet - MCD](/modélisation.png)

- _MembreBureau_ : Cette entité représente les membres du bureau de l’association.
Chaque membre a des attributs tels que l’ID, le nom, le prénom, etc.
Chaque membre du bureau a un compte utilisateur associé, ce qui signifie qu’ils ont des droits d’accès pour gérer le site.
- _CompteUtilisateur_ : Cette entité représente les comptes des utilisateurs du site. Chaque compte utilisateur a un rôle spécifique (par exemple, administrateur, partenaire, membre, etc.) qui détermine les droits d’accès sur le site. Par exemple, un administrateur peut avoir des droits pour créer des offres de stages.
- _OffreStages_ : Cette entité représente les offres de stages disponibles sur le site. Chaque offre est associée à un type d’offre spécifique et à un partenaire. Les offres contiennent des informations telles que le nom, la description, la date de début et de fin, etc.
- _Partenaire_ : Cette entité représente les partenaires qui proposent des offres de stages. Chaque partenaire peut avoir plusieurs offres, mais chaque offre est associée à un seul partenaire. Les partenaires ont des attributs tels que le nom, le logo, la description, etc.
- _TypeOffre_ & _TypeEvenement_ : Ces entités représentent les différents types d’offres et d’événements disponibles sur le site. Par exemple, le type d’offre pourrait être “stage”, “emploi”, “alternance”, etc. Le type d’événement pourrait être “after-work”, “rencontre”, “challenge”, etc.
- _Evenement_ : Cette entité représente les événements organisés par l’association. Chaque événement est associé à un type d’événement spécifique. Les événements ont des attributs tels que le nom, la date, fiche de l'événement, etc.
- _Contacte_ : Cette entité représente les contacts de chaque partenaire.


***

## Exemple des pages du site :
![Exemple des pages du site](/interfaces.png)

### Enjoy !


***
