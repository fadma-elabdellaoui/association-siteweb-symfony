FROM php:8-fpm-alpine3.19
ARG GOODUSER
ARG UID
ARG MAIL
ARG NOM

# installation bash
RUN apk --no-cache update && apk --no-cache add bash npm
RUN set -ex \
	&& apk --no-cache add postgresql-libs postgresql-dev \
	&& docker-php-ext-install pgsql pdo_pgsql\
	&& apk del postgresql-dev

# installation de composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php composer-setup.php --install-dir=/usr/local/bin \
&& php -r "unlink('composer-setup.php');" \

RUN apk add --no-cache \
        acl \
        curl \
        fcgi \
        file \
        gettext \
            ;
COPY --from=mlocati/php-extension-installer:2 --link /usr/bin/install-php-extensions /usr/local/bin/

RUN set -eux; \
    install-php-extensions \
    apcu \
    gd \
    intl \
    ;

# installation de symfony
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash
RUN apk add symfony-cli

# Gestion user
# RUN addgroup -g $GID -S $GROUP
RUN adduser -h /home/$GOODUSER -D -s /bin/bash -u $UID $GOODUSER
USER $GOODUSER

WORKDIR /var/www/html
