<?php

namespace App\Entity;

use App\Entity\Offre;
use App\Repository\LangageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[UniqueEntity('nomLangage',message: 'Cette compétence est déjà enregistrée.')]
#[ORM\Entity(repositoryClass: LangageRepository::class)]
class Langage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nomLangage = null;

    #[ORM\ManyToMany(targetEntity: Offre::class, mappedBy: 'langages')]
    private Collection $offres;

    public function __construct()
    {
        $this->offres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomLangage(): ?string
    {
        return $this->nomLangage;
    }

    public function setNomLangage(string $nomLangage): static
    {
        $this->nomLangage = $nomLangage;

        return $this;
    }

    /**
     * @return Collection<int, Offre>
     */
    public function getOffres(): Collection
    {
        return $this->offres;
    }

    public function addOffre(Offre $offre): static
    {
        if (!$this->offres->contains($offre)) {
            $this->offres->add($offre);
            $offre->addLangage($this);
        }

        return $this;
    }

    public function removeOffre(Offre $offre): static
    {
        if ($this->offres->removeElement($offre)) {
            $offre->removeLangage($this);
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getNomLangage();
    }
}
