<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager): void
    {
        // creation du compte Admin :
        $admin = new Utilisateur();
        $password = $this->hasher->hashPassword($admin,'secret');
        $admin->setUsername("Admin")
            ->setPassword($password)
            ->setRoles(array('ROLE_ADMIN'))
            ->setNom("Admin")
            ->setPrenom("Admin");
        $manager->persist($admin);
        $manager->flush();
    }
}
