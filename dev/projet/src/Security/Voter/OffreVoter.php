<?php

namespace App\Security\Voter;

use App\Entity\Offre;
use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OffreVoter extends Voter
{
    public const ALLOW = 'OFFRE_EDIT';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return
                $attribute == self::ALLOW
                && $subject instanceof \App\Entity\Offre;
    }

    /**
     * @param Offre|null $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof Utilisateur) {
            return false;
        }

        switch ($attribute) {
            case self::ALLOW:
                return $subject->getCreateur()->getId() === $user->getId() || in_array('ROLE_ADMIN', $user->getRoles());
                break;
        }
        return false;
    }
}
