<?php

namespace App\Security\Voter;

use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class IndexOffreVoter extends Voter
{
    public const VIEW = 'OFFRE_VIEW';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array($attribute, [self::VIEW])
            && $subject instanceof \App\Entity\Offre;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof Utilisateur) {
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                if ( in_array('ROLE_ADMIN', $user->getRoles()) ||  $subject->getCreateur()->getId() === $user->getId() ){
                    return true;
                }
                break;
        }
        return false;
    }
}
