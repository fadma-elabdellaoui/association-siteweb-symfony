<?php

namespace App\Security\Voter;

use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class NewOffreVoter extends Voter
{
    public const NEW = 'OFFRE_NEW';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute == self::NEW;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof Utilisateur) {
            return false;
        }

        switch ($attribute) {

            case self::NEW:
                if ( in_array('ROLE_PARTENAIRE_ESN', $user->getRoles()) || in_array('ROLE_ADMIN', $user->getRoles())){
                    return true;
                }
                break;
        }
        return false;
    }
}
