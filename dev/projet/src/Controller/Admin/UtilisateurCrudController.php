<?php

namespace App\Controller\Admin;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class UtilisateurCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Utilisateur::class;
    }

    public function __construct(
        public UserPasswordHasherInterface $userPasswordHasher
    ) {}

    public function configureFields(string $pageName): iterable
    {
        $nom = TextField::new('nom');
        $prenom = TextField::new('prenom');
        $userName = TextField::new('username');

        $password = TextField::new('password')
            ->setFormType(RepeatedType::class)
            ->setFormTypeOptions([
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Confirm Password'],
                'mapped' => false,
            ])
            ->setRequired(true)
            ->onlyOnForms();

        $roles = ChoiceField::new('roles', 'Roles')
            ->allowMultipleChoices()
            ->setChoices([ 'Admin' => 'ROLE_ADMIN', 'PartenaireESN' => 'ROLE_PARTENAIRE_ESN'])
            ->setSortable(false)
            ->setPermission('ROLE_ADMIN');

        $listeOffresCree = ArrayField::new('offresStageCree')->onlyOnDetail();

        if (Crud::PAGE_INDEX === $pageName) {
            return [$nom, $prenom, $userName, $roles];
        } elseif(Crud::PAGE_NEW === $pageName || Crud::PAGE_EDIT === $pageName) {
            return [$nom, $prenom, $userName, $password, $roles];
        }else{
            return [$nom, $prenom, $userName, $roles,$listeOffresCree];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        $annuler = Action::new(Action::INDEX)
            ->setCssClass('btn btn-danger text-white')
            ->setLabel('Annuler')
            ->linkToCrudAction(Action::INDEX)
            ;

        $actions
            ->add(Crud::PAGE_EDIT,$annuler)
            ->add(Crud::PAGE_NEW,$annuler)
            ->setPermission(Action::INDEX, 'ROLE_ADMIN')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setLabel('Voir');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })->setPermission(Action::DELETE,'ROLE_ADMIN')
            ->update(Crud::PAGE_INDEX,Action::NEW,function (Action $action) {
                return $action->setLabel('Ajouter');
            })->setPermission(Action::NEW, 'ROLE_ADMIN')
            ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::INDEX])
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_NEW,Action::SAVE_AND_ADD_ANOTHER)
            ->update(Crud::PAGE_DETAIL,Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })
            ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                return $action->setLabel('Revenir à la liste');
            })
            ->reorder(Crud::PAGE_DETAIL,[Action::EDIT,Action::DELETE,Action::INDEX])
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_EDIT,Action::SAVE_AND_CONTINUE)
            ->reorder(Crud::PAGE_EDIT,[Action::SAVE_AND_RETURN,Action::INDEX])

        ;
        return $actions;
    }
    private function addPasswordEventListener(FormBuilderInterface $formBuilder): FormBuilderInterface
    {
        return $formBuilder->addEventListener(FormEvents::POST_SUBMIT, $this->hashPassword());
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        return $this->addPasswordEventListener($formBuilder);
    }
    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        return $this->addPasswordEventListener($formBuilder);
    }
    private function hashPassword() {
        return function($event) {
            $form = $event->getForm();
            if (!$form->isValid()) {
                return;
            }
            $password = $form->get('password')->getData();
            if ($password === null) {
                return;
            }

            $hash = $this->userPasswordHasher->hashPassword($form->getData(), $password);
            $form->getData()->setPassword($hash);

        };
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Utilisateur')
            ->setEntityLabelInPlural('Utilisateurs')
            ->setPageTitle(Crud::PAGE_NEW,"Ajouter un utilisateur")
            ->setPageTitle(Crud::PAGE_EDIT,"Modifier utilisateur")
            ;
    }

}
