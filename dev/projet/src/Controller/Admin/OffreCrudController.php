<?php

namespace App\Controller\Admin;

use App\Entity\Offre;
use App\Security\Voter\NewOffreVoter;
use App\Security\Voter\OffreVoter;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;

class OffreCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Offre::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre'),
            AssociationField::new('typeOffre')->setRequired(true),
            TextareaField::new('description'),
            NumberField::new('duree')->setLabel('Durée'),
            DateField::new('dateDebut')->setLabel('Date début'),
            AssociationField::new('partenaire'),
            AssociationField::new('langages'),
            ArrayField::new('langages')->onlyOnDetail(),
            AssociationField::new('createur')->setLabel('Crée par')->onlyOnIndex()->setPermission('ROLE_ADMIN'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $annuler = Action::new(Action::INDEX)
            ->setCssClass('btn btn-danger text-white')
            ->setLabel('Annuler')
            ->linkToCrudAction(Action::INDEX)
        ;
        $actions
            ->setPermission(Crud::PAGE_INDEX, 'ROLE_PARTENAIRE_ESN')
            ->add(Crud::PAGE_EDIT,$annuler)
            ->add(Crud::PAGE_NEW,$annuler)

            ->update(Crud::PAGE_INDEX,Action::NEW,function (Action $action) {
                return $action->setLabel('Ajouter');
            })->setPermission(Action::NEW, permission: NewOffreVoter::NEW)

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })->setPermission(Action::EDIT,OffreVoter::ALLOW)

            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })->setPermission(Action::DELETE,OffreVoter::ALLOW)

            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setLabel('Voir');
            })->setPermission(Action::DETAIL,OffreVoter::ALLOW)

            ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::INDEX])
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_NEW,Action::SAVE_AND_ADD_ANOTHER)

            ->update(Crud::PAGE_DETAIL,Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })
            ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                return $action->setLabel('Revenir à la liste');
            })
            ->reorder(Crud::PAGE_DETAIL,[Action::EDIT,Action::DELETE,Action::INDEX])

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_EDIT,Action::SAVE_AND_CONTINUE)
            ->reorder(Crud::PAGE_EDIT,[Action::SAVE_AND_RETURN,Action::INDEX])
        ;
        return $actions;
    }

    private function addCreateurEventListener(FormBuilderInterface $formBuilder): FormBuilderInterface
    {
        return $formBuilder->addEventListener(FormEvents::POST_SUBMIT, $this->addCreateur());
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        return $this->addCreateurEventListener($formBuilder);
    }
    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        return $this->addCreateurEventListener($formBuilder);
    }
    private function addCreateur() {
        return function($event) {
            $form = $event->getForm();
            if (!$form->isValid()) {
                return;
            }
            $form->getData()->setCreateur($this->getUser());
        };
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Offre')
            ->setEntityLabelInPlural('Offres')
            ->setPageTitle(Crud::PAGE_NEW,"Ajouter une offre")
            ->setPageTitle(Crud::PAGE_EDIT,"Modifier offre")
            ->setPageTitle(Crud::PAGE_DETAIL,"Offre")
            ;
    }
}
