<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Entity\Evenement;
use App\Entity\Langage;
use App\Entity\MembreBureau;
use App\Entity\Offre;
use App\Entity\Partenaire;
use App\Entity\PartenaireAnnuel;
use App\Entity\TypeEvenement;
use App\Entity\TypeOffre;
use App\Entity\Utilisateur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
         $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
         return $this->redirect($adminUrlGenerator->setController(OffreCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<h4 class="mt-2 fw-bold text-center" style="color: #4C59AB;">Back Office</h4>')
            ->setFaviconPath('images/AMIGOFinal.webp')
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Entités');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-users', Utilisateur::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Membres du Bureau', 'fas fa-users-line', MembreBureau::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Entreprises ESN', 'fas fa-building', Partenaire::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Contacts', 'fas fa-user-tie', Contact::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Partenaires Annuels', 'fa-regular fa-handshake', PartenaireAnnuel::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('Offres');
        yield MenuItem::linkToCrud('Offres', 'fas fa-list', Offre::class)->setPermission('ROLE_PARTENAIRE_ESN');
        yield MenuItem::linkToCrud('Type d\'offre', 'fas fa-tag', TypeOffre::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Compétences', 'fas fa-screwdriver-wrench', Langage::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('Evénements');
        yield MenuItem::linkToCrud('Evénements', 'fas fa-list', Evenement::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Type d\'evenement', 'fas fa-tag', TypeEvenement::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::linkToUrl('Voir le site', 'fas fa-globe', $this->generateUrl('index'));
    }


    public function configureUserMenu(UserInterface $user): UserMenu
    {
        $admUrlGenerate = $this->container->get(AdminUrlGenerator::class);
        $targetUrl = $admUrlGenerate
            ->setController(UtilisateurCrudController::class)
            ->setAction(Crud::PAGE_EDIT)
            ->setEntityId($user->getId())
            ->generateUrl();

        $userMenuItems = [
            //MenuItem::linkToUrl('Profile','fa-id-card',$targetUrl),
            MenuItem::linkToLogout('Se déconnecter', 'fa-sign-out')
        ];

        if ($this->isGranted(Permission::EA_EXIT_IMPERSONATION)) {
            $userMenuItems[] =
                MenuItem::linkToExitImpersonation(
                    'Se déconnecter',
                    'fa-user-lock'
                );
        }

        return UserMenu::new()
            ->displayUserName()
            ->setName(method_exists($user, '__toString') ? (string) $user : $user->getUsername())
            ->setMenuItems($userMenuItems);
    }

}
