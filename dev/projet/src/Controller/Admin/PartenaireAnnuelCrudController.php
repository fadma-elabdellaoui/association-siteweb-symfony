<?php

namespace App\Controller\Admin;

use App\Entity\PartenaireAnnuel;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PartenaireAnnuelCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PartenaireAnnuel::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('adresse'),
            TextareaField::new('description')->setLabel('Description de l\'offre'),
            ImageField::new('logo','logo')
                ->setUploadDir('public/uploads/images/logosPartenaires')
                ->setBasePath('uploads/images/logosPartenaires'),
        ];
    }
    public function configureActions(Actions $actions): Actions
    {
        $annuler = Action::new(Action::INDEX)
            ->setCssClass('btn btn-danger text-white')
            ->setLabel('Annuler')
            ->linkToCrudAction(Action::INDEX)
        ;

        $actions
            ->add(Crud::PAGE_EDIT,$annuler)
            ->add(Crud::PAGE_NEW,$annuler)
            ->setPermission(Action::INDEX, 'ROLE_ADMIN')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setLabel('Voir');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })->setPermission(Action::DELETE,'ROLE_ADMIN')
            ->update(Crud::PAGE_INDEX,Action::NEW,function (Action $action) {
                return $action->setLabel('Ajouter');
            })->setPermission(Action::NEW, 'ROLE_ADMIN')
            ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::INDEX])
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_NEW,Action::SAVE_AND_ADD_ANOTHER)
            ->update(Crud::PAGE_DETAIL,Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })
            ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                return $action->setLabel('Revenir à la liste');
            })
            ->reorder(Crud::PAGE_DETAIL,[Action::EDIT,Action::DELETE,Action::INDEX])
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->remove(Crud::PAGE_EDIT,Action::SAVE_AND_CONTINUE)
            ->reorder(Crud::PAGE_EDIT,[Action::SAVE_AND_RETURN,Action::INDEX])
        ;
        return $actions;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Partenaire Annuel')
            ->setEntityLabelInPlural('Partenaires annuels')
            ->setPageTitle(Crud::PAGE_NEW,"Ajouter un partenaire annuel")
            ->setPageTitle(Crud::PAGE_EDIT,"Modifier partenaire annuel")
            ->setPageTitle(Crud::PAGE_DETAIL,"Partenaire Annuel")
            ;
    }
}
