<?php

namespace App\Controller;
use App\Entity\Langage;
use App\Entity\Partenaire;
use App\Entity\TypeOffre;
use App\Repository\EvenementRepository;
use App\Repository\LangageRepository;
use App\Repository\MembreBureauRepository;
use App\Repository\PartenaireAnnuelRepository;
use App\Repository\PartenaireRepository;
use App\Repository\OffreRepository;

use App\Repository\TypeOffreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }
    #[Route('/presentation', name: 'presentation')]
    public function presentation(): Response
    {
        return $this->render('pages/presentation.html.twig');
    }
    #[Route('/entreprises', name: 'app_partenaire_index', methods: ['GET'])]
    public function indexPartenaire(PartenaireRepository $partenaireRepository): Response
    {
        return $this->render('pages/partenaires.html.twig', [
            'partenaires' => $partenaireRepository->findAll(),
        ]);
    }
    #[Route('/partenariats', name: 'app_partenariats_index', methods: ['GET'])]
    public function indexPartenariats(PartenaireAnnuelRepository $partenaireAnnuelRepository): Response
    {
        return $this->render('pages/partenariats.html.twig', [
            'partenaires' => $partenaireAnnuelRepository->findAll(),
        ]);
    }

    #[Route('/offres', name: 'app_offres_index', methods: ['GET'])]
    public function indexOffres(OffreRepository $offreRepository
                                ,PartenaireRepository $partenaireRepository
                                ,TypeOffreRepository $typeOffreRepository
                                ,LangageRepository $langageRepository): Response
    {
        return $this->render('pages/offres.html.twig', [
            'offres' => $offreRepository->findAll(),
            'entreprises' => $partenaireRepository->findAll(),
            'typesOffre' => $typeOffreRepository->findAll(),
            'langages' => $langageRepository->findAll(),
        ]);
    }

    #[Route('/membres', name: 'app_membres_index', methods: ['GET'])]
    public function membres(MembreBureauRepository $membreBureauRepository): Response
    {
        return $this->render('pages/membres.html.twig', [
            'membres' => $membreBureauRepository->findAll(),
        ]);
    }

    #[Route('/evenements', name: 'app_evenements_index', methods: ['GET'])]
    public function evenements(EvenementRepository $evenementRepository): Response
    {
        return $this->render('pages/evenements.html.twig', [
            'evenements' => $evenementRepository->findAll(),
        ]);
    }
    #[Route('/offres/partenaire/{id}', name: 'app_offre_partenaire', methods: ['GET'])]
    public function indexByPartenaire(Partenaire $partenaire
                                        ,Request $request
                                        ,OffreRepository $offreRepository
                                        ,PartenaireRepository $partenaireRepository
                                        ,TypeOffreRepository $typeOffreRepository
                                        ,LangageRepository $langageRepository): Response
    {
        return $this->render('pages/offres.html.twig', [
            'offres' => $offreRepository->findBy(['partenaire' => $partenaire],[]),
            'entreprises' => $partenaireRepository->findAll(),
            'typesOffre' => $typeOffreRepository->findAll(),
            'langages' => $langageRepository->findAll(),
        ]);
    }
    #[Route('/offres/type/{typeOffre}', name: 'app_offre_type', methods: ['GET'])]
    public function indexByTypeOffre(TypeOffre $typeOffre
                                        ,Request $request
                                        ,OffreRepository $offreRepository
                                        ,PartenaireRepository $partenaireRepository
                                        ,TypeOffreRepository $typeOffreRepository
                                        ,LangageRepository $langageRepository): Response
    {
        return $this->render('pages/offres.html.twig', [
            'offres' => $offreRepository->findBy(['typeOffre' => $typeOffre],[]),
            'entreprises' => $partenaireRepository->findAll(),
            'typesOffre' => $typeOffreRepository->findAll(),
            'langages' => $langageRepository->findAll()
        ]);
    }

}