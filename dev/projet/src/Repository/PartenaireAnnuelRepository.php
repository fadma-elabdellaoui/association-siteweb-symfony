<?php

namespace App\Repository;

use App\Entity\PartenaireAnnuel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PartenaireAnnuel>
 *
 * @method PartenaireAnnuel|null find($id, $lockMode = null, $lockVersion = null)
 * @method PartenaireAnnuel|null findOneBy(array $criteria, array $orderBy = null)
 * @method PartenaireAnnuel[]    findAll()
 * @method PartenaireAnnuel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartenaireAnnuelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PartenaireAnnuel::class);
    }

    //    /**
    //     * @return PartenaireAnnuel[] Returns an array of PartenaireAnnuel objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PartenaireAnnuel
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
